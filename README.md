# RNotebookTest
Steps to add new pages
1. clone the project, and make a new branch
    ```
    git clone git@gitlab.com:cgps/ghru/Rexamples.git
    cd Rexamples
    git checkout -b my_name
    ```
2. Edit existing pages e.g `data_joining_2.Rmd`    

3. Or to add new pages edit the `_site.yml` file adding a new item to the `left` list.
    ```
      left:
      - text: "Data Joining 1"
        href: data_joining_1.html
      - text: "Data Joining 2"
        href: data_joining_2.html
    ```
    Then make a new `.Rmd` file corresponding to the html file. e.g `data_joining_3.html` => `data_joining_3.Rmd`

4. Make the HTML pages from the Rmd files using the command `rmarkdown::render_site()`
5. Commit changes and push to gitlab
    ```
    git add .
    git commit -m 'new examples'
    git push -u origin my_name
    ```
6. Make a merge request on gitlab to merge these changes to master

Once the merge request has been merged, the new pages will appear at this [link](https://cgps.gitlab.io/ghru/Rexamples/)