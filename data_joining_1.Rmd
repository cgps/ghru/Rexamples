---
title: "Manipulating AMR metadata"
author: "Anthony Underwood"
---
## Summary
This example demonstates methods for combining and grouping data from the GHRU metadata.
The starting point is the AMR and epi data uploaded to the Googlesheets using Data-flo. The AMR sheets have a lot of information and it is difficult to assess the various classes of AMR determinants.

### Getting the data

---

First load required libraries
```{r message=FALSE, warning=FALSE}
library(tidyverse)
library(kableExtra)
library(janitor)
library(magrittr)
```

Get Kpn epi and amr data from generated from India
```{r warning=FALSE, message=FALSE}
epi_data <- ghruR::get_data_for_country(country_value = "India", type_value = "Epidemiological Metadata", user_email = "au3@sanger.ac.uk")
acquired_amr_data <- ghruR::get_data_for_country(country_value = "India", type_value = "AMR Klebsiella pneumoniae", AMR_type = "acquired", user_email = "au3@sanger.ac.uk")
head(acquired_amr_data) %>% kable() %>% kable_styling() %>% scroll_box(width = "100%")
point_amr_data <- ghruR::get_data_for_country(country_value = "India", type_value = "AMR Klebsiella pneumoniae", AMR_type = "point", user_email = "au3@sanger.ac.uk")
head(point_amr_data) %>% kable() %>% kable_styling() %>% scroll_box(width = "100%")
```
### Combining columns to have one column for each major drug class

---

As it stands we have ARIBA gene and mutation presence and absence where the column names are cluster names. These are not always informative. We can improve this by combining the matches with metadata that describes the acquired gene or mutations.

#### Get meta data about acquired genes from the NCBI AMR gene catalogue
```{r message=FALSE, warning=FALSE}
ncbi_acquired_gene_metadata <- readr::read_tsv("https://ftp.ncbi.nlm.nih.gov/pathogen/Antimicrobial_resistance/Data/latest/ReferenceGeneCatalog.txt")
head(ncbi_acquired_gene_metadata) %>% kable() %>% kable_styling() %>% scroll_box(width = "100%")
```
#### Get point mutation metadata from the ariba database files
```{r message=FALSE, warning=FALSE}
  # Get data about point muations for the speaces
  point_mutation_metadata <- readr::read_tsv("https://gitlab.com/cgps/ghru/pipelines/data_sources/ariba_amr_databases/-/raw/master/pointfinder/klebsiella_db/01.filter.check_metadata.tsv", col_names = FALSE)
  # Add column names based on ariba docs
  colnames(point_mutation_metadata) <- c("sequence_name", "gene?", "variant?", "mutation", "gene", "resistance")
  # extract inferred resistance and name the column resistance.
  # Make a column named mutation by combining the gene and existing mutation column
  # Discard all the other columns
  point_mutation_metadata %<>% dplyr::mutate(resistance = paste0("point_", stringr::str_match(resistance, "^Resistance: (.+?)\\.")[,2]))  %>% 
    tidyr::unite(mutation, c(gene,mutation), sep = "_") %>% 
    dplyr::select(mutation, resistance)
  head(point_mutation_metadata) %>% kable() %>% kable_styling() 
```
### Processing acquired AMR data

---


The acquired data and point data need to be processed separately. Taking acquired data first. The main objective in most of the steps described below is to have tidy data as described in this [page](https://cfss.uchicago.edu/notes/tidy-data/) and implemented through the amazing tidyverse suite of libraries.
```{r message=FALSE, warning=FALSE}
# make data long so that for each gene there is a single row for each observation of a gene in a gene cluster
long_amr_data <- acquired_amr_data %>%
  dplyr::select(-`Alternative sample id`) %>%
  tidyr::pivot_longer(-`Sample id`, names_to = 'metric', values_to = 'value') %>%
  tidyr::extract(col = 'metric', into = c('ariba_cluster', 'metric'), regex = '^([^\\.]+)\\.(.+)', remove = TRUE)%>%
  tidyr::pivot_wider(names_from = 'metric', values_from = 'value')

# filter out only good results based on assembly status (starts with yes),convert ctg_cov to float and filter for depth of coverage > 10
filtered_long_amr_data <- long_amr_data %>%
  filter(stringr::str_starts(assembled, "yes"))

filtered_long_amr_data  %<>% na_if("NA") %>%
  mutate(ctg_cov = as.numeric(ctg_cov)) %>%
  filter(ctg_cov > 10)

# select only Sample id and accession columns and extract just the accession from the ref_seq column, renaming it to refseq_nucleotide_accession
filtered_long_amr_data  %<>%
  select(`Sample id`, ref_seq) %>%
  mutate(ref_seq = stringr::str_extract(ref_seq, "NG_.+$")) %>%
  rename(refseq_nucleotide_accession = ref_seq)

# select only the required columns from the NCBI metadata
ncbi_acquired_gene_metadata  %<>% 
  filter(type == "AMR") %>% 
  select(refseq_nucleotide_accession, allele, gene_family, class,subclass) %>% 
  drop_na(allele) %>%
  drop_na(gene_family) %>% 
  drop_na(refseq_nucleotide_accession)


# combine amr data with NCBI metadata using the refseq_nucleotide_accession to join the tables
annotated_acquired_amr_data <- filtered_long_amr_data %>%
  left_join(ncbi_acquired_gene_metadata, by = "refseq_nucleotide_accession")

# Some deyerminants do not have alleles and therefore the allele and gene_family should be merged into one column where allele does not exist. THe coalesce function can be used for this
annotated_acquired_amr_data %<>% dplyr::mutate(gene = dplyr::coalesce(allele, gene_family)) %>%
  select(-c(allele, gene_family))

head(annotated_acquired_amr_data) %>% kable() %>% kable_styling() %>% scroll_box(width = "100%")
```
For the acquired genes the NCBI subclasses have many different variants and some should be rationalised. A suggestion would be to use an external table as a source for combining. Here is an example hosted in a [Google Sheet](https://docs.google.com/spreadsheets/d/1X4-xD2-e78geJOFLOKri2QjeOzhrPLHzwsAN_f8mX1Q)

```{r echo=FALSE, message=FALSE, warning=FALSE}
knitr::include_graphics("images/NCBI_subclass_grouping.png")
```

This data can be used to group the subclasses
```{r message=FALSE, warning=FALSE}
# Get the subclass groups from the Google sheets
subclass_groups <- googlesheets4::read_sheet("https://docs.google.com/spreadsheets/d/1X4-xD2-e78geJOFLOKri2QjeOzhrPLHzwsAN_f8mX1Q")

# use this to merge subclasses
annotated_acquired_amr_data  %<>% 
  dplyr::left_join(subclass_groups %>% select(-class), by = "subclass") %>%
  dplyr::select(-subclass) %>%
  dplyr::rename(subclass = grouped_subclass)
head(annotated_acquired_amr_data) %>% kable() %>% kable_styling() %>% scroll_box(width = "100%")
```
Sometimes the class doesn't have a subclass. Therefore these columns should be combined using the coalesce funtion again.
```{r emessage=FALSE, warning=FALSE}
annotated_acquired_amr_data %<>% dplyr::mutate(class = dplyr::coalesce(subclass, class)) %>% 
  select(`Sample id`,gene,class)
head(annotated_acquired_amr_data) %>% kable() %>% kable_styling() %>% scroll_box(width = "100%")
```
Many samples will have more than one determinant for some classes. These should be combined before returning the data frame to wide format of one row per sample.

In the final data we will want something for every sample even if it's empty. This is the purpose of the final left join
```{r  message=FALSE, warning=FALSE}
# Use group_by and summarise functions to collapse multiple occurences of genes in the same class
annotated_acquired_amr_data %<>% dplyr::group_by(`Sample id`, class) %>%
  dplyr::summarise(gene = paste0(gene, collapse = ','))

# pivot this data back to wide format
wide_annotated_acquired_amr_data <- annotated_acquired_amr_data %>% 
  pivot_wider(names_from = c(class), values_from =c (gene))

# ensure all samples have data
wide_annotated_acquired_amr_data <- acquired_amr_data %>% dplyr::select(`Sample id`) %>% 
  dplyr::left_join(wide_annotated_acquired_amr_data, by = "Sample id")

head(wide_annotated_acquired_amr_data) %>% kable() %>% kable_styling() %>% scroll_box(width = "100%")
```

### Processing point AMR data

---

Use a similar process for point mutations. First convert to long format
```{r message=FALSE, warning=FALSE}
# pivot data into long tidy format
long_point_amr_data <- point_amr_data %>%
  dplyr::select(-`Alternative sample id`) %>%
  tidyr::pivot_longer(-`Sample id`, names_to = 'metric', values_to = 'value') %>%
  tidyr::extract(col = 'metric', into = c('gene', 'metric'), regex = '^([^\\.]+)\\.(.+)', remove = TRUE)

head(long_point_amr_data) %>% kable() %>% kable_styling() %>% scroll_box(width = "100%")
```

#### Process only those samples that have fully assembled genes
```{r message=FALSE, warning=FALSE}
# get just assembled metric
point_assembled_metrics <- long_point_amr_data %>%
  dplyr::filter(metric == 'assembled') %>%
  dplyr::rename(assembled = value) %>%
  dplyr::select(-metric)

# find those that are assembled fully
fully_assembled_point_genes <- point_assembled_metrics  %>%
  dplyr::filter(stringr::str_starts(assembled, "yes")) %>%
  dplyr::select(-assembled)

# get the point mutations
point_mutations <- long_point_amr_data %>%
  dplyr::filter(metric != 'assembled')

# link fully assembled genes to mutations and to their respective associated AMR
fully_assembled_point_mutations <- fully_assembled_point_genes %>%
  dplyr::inner_join(point_mutations, by = c('Sample id', 'gene')) %>%
  dplyr::rename(mutation = metric, mutated = value) %>%
  dplyr::filter(mutated == "yes") %>%
  dplyr::select(-mutated) %>%
  tidyr::unite(determinant, gene, mutation)
# link to their respective associated resistances
fully_assembled_point_mutations %<>% dplyr::left_join(point_mutation_metadata, by = c("determinant" = "mutation"))

# As before unite multiple occurences for the same Sample/resistance combination
fully_assembled_point_mutations %<>% dplyr::group_by(`Sample id`, resistance) %>%
  dplyr::summarise(determinant = paste0(determinant, collapse = ','))

# Make wide
wide_point_mutations <- fully_assembled_point_mutations %>% 
  pivot_wider(names_from = c(resistance), values_from =c (determinant))

# ensure all samples have data
wide_point_mutations <- amr_data %>% dplyr::select(`Sample id`) %>% 
  dplyr::left_join(wide_point_mutations, by = "Sample id")

head(wide_point_mutations) %>% kable() %>% kable_styling() %>% scroll_box(width = "100%")
```
Include the data from incomplete genes
```{r message=FALSE, warning=FALSE}
incomplete_point_genes <- point_assembled_metrics  %>%
  filter(!stringr::str_starts(assembled, "yes"))

point_mutation_genes <- point_mutation_metadata %>%
  dplyr::mutate(gene = stringr::str_replace(mutation, "_.+$", "")) %>%
  dplyr::select(gene, resistance) %>%
  dplyr::distinct(gene, resistance)

incomplete_point_genes %<>% 
  dplyr::inner_join(point_mutation_genes, by = "gene") %>%
  dplyr::mutate(assembled = stringr::str_replace(assembled, "no", "absent")) %>%
  tidyr::unite(determinant, gene, assembled, sep = "-")

# As before unite multiple occurences for the same Sample/resistance combination
incomplete_point_genes %<>% dplyr::group_by(`Sample id`, resistance) %>%
  dplyr::summarise(determinant = paste0(determinant, collapse = ','))

# to distinguish these in the final table. Change the name from point_XYZ to incomplete_point_XYZ
incomplete_point_genes %<>% dplyr::mutate(resistance = paste0(resistance, "_incomplete"))

# Make wide
wide_incomplete_point_mutations_genes <- incomplete_point_genes %>% 
  pivot_wider(names_from = c(resistance), values_from =c (determinant))

# ensure all samples have data
wide_incomplete_point_mutations_genes <- point_amr_data %>% dplyr::select(`Sample id`) %>% 
  dplyr::left_join(wide_incomplete_point_mutations_genes, by = "Sample id")

head(wide_incomplete_point_mutations_genes) %>% kable() %>% kable_styling() %>% scroll_box(width = "100%")
```

### Merge the data

---

```{r message=FALSE, warning=FALSE}
# First merge the point data so that the headings can be sorted
combined_point_data <- wide_point_mutations %>% 
  dplyr::left_join(wide_incomplete_point_mutations_genes, by = "Sample id")

# sort columns
colnames_without_id <- sort(setdiff(colnames(combined_point_data), c('Sample id')))
combined_point_data %<>% dplyr::select(`Sample id`, all_of(colnames_without_id))

# remove all empty columns
combined_point_data %>% janitor::remove_empty()

# Now merge with epi data and acquired data
combined_data <- acquired_amr_data %>% dplyr::select(`Sample id`) %>% 
   dplyr::left_join(epi_data, by = "Sample id") %>% 
  dplyr::left_join(wide_annotated_acquired_amr_data, by = "Sample id") %>% 
  dplyr::left_join(combined_point_data, by = "Sample id")
head(combined_data) %>% kable() %>% kable_styling()
```

### Adding colours for Microreact

---

For this it is necessary to go back to the wide data and make it long again, colour based on presence of a determinant ( e.g `dplyr::mutate(colour = if_else(is.na(determinant), "white", "red"))`)  and finally combine it back again to a wide format suitable for microreact.
```{r message=FALSE, warning=FALSE}
##### colour acquired mutation metadata
# make long
long_annotated_acquired_amr_data <- wide_annotated_acquired_amr_data %>% 
  tidyr::pivot_longer(-`Sample id`, names_to = "class", values_to = "determinant")
# add a column for subclass__colour
long_annotated_acquired_amr_data %<>% dplyr::mutate(class__colour = stringr::str_c(class, "__colour"))
# add values for colour
long_annotated_acquired_amr_data %<>% dplyr::mutate(colour = if_else(is.na(determinant), "white", "red"))
# pivot the subclass and gene data into wide format
long_annotated_acquired_amr_data_class <- long_annotated_acquired_amr_data %>% dplyr::select(`Sample id`, class, determinant)
wide_annotated_acquired_amr_data_class <- long_annotated_acquired_amr_data_class %>%
  pivot_wider(names_from = c(class), values_from =c (determinant))
# pivot the subclass__colour and colour data into wide format
long_annotated_acquired_amr_data_colour <- long_annotated_acquired_amr_data %>% dplyr::select(`Sample id`, class__colour, colour)
wide_annotated_acquired_amr_data_colour <- long_annotated_acquired_amr_data_colour %>%
  pivot_wider(names_from = c(class__colour), values_from =c (colour))
# merge the two wide tables
wide_combined_acquired <- wide_annotated_acquired_amr_data_class %>% dplyr::left_join(wide_annotated_acquired_amr_data_colour, by = "Sample id")
# sort columns
colnames_without_id <- sort(setdiff(colnames(wide_combined_acquired), c('Sample id')))
wide_combined_acquired %<>% dplyr::select(`Sample id`, all_of(colnames_without_id))
head(combined_data) %>% kable() %>% kable_styling()
```

Colour the point mutation data
```{r message=FALSE, warning=FALSE}
##### colour point mutation metadata
# make long
long_point_mutations <- wide_point_mutations %>% 
  tidyr::pivot_longer(-`Sample id`, names_to = "point_resistance", values_to ="mutation")
# add a column for point__colour
long_point_mutations %<>% dplyr::mutate(point_resistance__colour = stringr::str_c(point_resistance, "__colour"))
long_point_mutations %<>% dplyr::mutate(colour = if_else(is.na(mutation), "white", "red"))
# pivot the resistance and gene data into wide format
long_point_mutations_resistance <- long_point_mutations %>% dplyr::select(`Sample id`, point_resistance, mutation)
wide_point_mutations_resistance <- long_point_mutations_resistance %>%
  pivot_wider(names_from = c(point_resistance), values_from =c (mutation))
# pivot the subclass__colour and colour data into wide format
long_point_mutations_colour <- long_point_mutations %>% dplyr::select(`Sample id`, point_resistance__colour, colour)
wide_point_mutations_colour <- long_point_mutations_colour %>% 
  pivot_wider(names_from = c(point_resistance__colour), values_from =c (colour))


long_incomplete_point_mutations_genes <- wide_incomplete_point_mutations_genes %>% 
  tidyr::pivot_longer(-`Sample id`, names_to = "point_resistance", values_to ="gene")
# add a column for point__colour
long_incomplete_point_mutations_genes %<>% dplyr::mutate(point_resistance__colour = stringr::str_c(point_resistance, "__colour"))
long_incomplete_point_mutations_genes %<>% dplyr::mutate(colour = if_else(is.na(gene), "white", "orange"))
# pivot the resistance and gene data into wide format
long_incomplete_point_mutations_genes_type <- long_incomplete_point_mutations_genes %>% dplyr::select(`Sample id`, point_resistance, gene)
wide_incomplete_point_mutations_genes_type <- long_incomplete_point_mutations_genes_type %>%
  pivot_wider(names_from = c(point_resistance), values_from =c (gene))
# pivot the subclass__colour and colour data into wide format
long_incomplete_point_mutations_genes_colour <- long_incomplete_point_mutations_genes %>% dplyr::select(`Sample id`, point_resistance__colour, colour)
wide_incomplete_point_mutations_genes_colour <- long_incomplete_point_mutations_genes_colour %>%
  pivot_wider(names_from = c(point_resistance__colour), values_from =c (colour))

# merge the four wide tables
wide_combined_point <- wide_point_mutations_resistance %>%
  dplyr::left_join(wide_point_mutations_colour, by = c('Sample id')) %>%
  dplyr::left_join(wide_incomplete_point_mutations_genes_type, by = c('Sample id')) %>%
  dplyr::left_join(wide_incomplete_point_mutations_genes_colour, by = c('Sample id')) 

# sort columns
colnames_without_id <- sort(setdiff(colnames(wide_combined_point), c('id')))
wide_combined_point %<>% dplyr::select(`Sample id`, all_of(colnames_without_id))

######## merge all data together #####################
combined_coloured_data <- acquired_amr_data %>% dplyr::select(`Sample id`) %>%
   dplyr::left_join(epi_data, by = c('Sample id')) %>% 
  dplyr::left_join(wide_combined_acquired, by = c('Sample id')) %>% 
  dplyr::left_join(wide_combined_point, by = c('Sample id'))
head(combined_coloured_data) %>% kable() %>% kable_styling() %>% scroll_box(width = "100%")

```

## Splitting out columns

---

The subclass columns are very broad and sometimes it nay be helpful to split them out.
Below are a couple of functions that will help with to do this and can be adjusted to your own use case

This first function will take a specified column and split into either one column per allele (if no `allele_gene_separator` argument is provided) or one column per gene. In the case of the NCBI aquired gene database the gene name and allele_number are separated by a `_` character so this will be used
```{r message=FALSE, warning=FALSE}
split_consolidated_column <- function(metadata, column_name, id_column = 'id', allele_gene_separator = NA){
  selected_columns <- metadata %>% dplyr::select(id_column, column_name)
  max_num_genes <- selected_columns %>% dplyr::pull(!!column_name) %>% na.omit %>% str_count(pattern = ",") %>% max() + 1
  split_data <- selected_columns %>% tidyr::separate(!!column_name, into = paste0("", 1:max_num_genes), sep = ",") 
  split_data_long <- split_data %>% pivot_longer(-id_column, names_to = "number", values_to = "allele") %>% 
    dplyr::select(-number) %>%
    dplyr::distinct()
  if (!is.na(allele_gene_separator)){
    split_data_long  %<>% mutate(gene = stringr::str_match(allele, paste0("^(.+)", allele_gene_separator))[,2])
    split_data_wide <- split_data_long %>% tidyr::pivot_wider(names_from = c(gene), values_from =c(allele)) %>%
      dplyr::select(-`NA`)
  } else {
    split_data_long  %<>% mutate(presence = if_else(is.na(allele), "no", "yes"))
    split_data_wide <- split_data_long %>% tidyr::pivot_wider(names_from = c(allele), values_from =c(presence)) %>% 
      dplyr::select(-`NA`) %>% 
      dplyr::mutate(across(everything(), replace_na, "no"))
  }
  # sort
  colnames_without_id <- sort(setdiff(colnames(split_data_wide), c(id_column)))
  split_data_wide %<>% dplyr::select(id_column, all_of(colnames_without_id))
  return(split_data_wide)
}
```

To demonstrate this the `CARBAPENEM` column will be split first into one column per allele
```{r message=FALSE, warning=FALSE}
split_data <- split_consolidated_column(combined_data, 'CARBAPENEM', id_column = 'Sample id')
head(split_data) %>% kable() %>% kable_styling() %>% scroll_box(width = "100%")
```
Secondly into one column per gene
```{r message=FALSE, warning=FALSE}
split_data <- split_consolidated_column(combined_data, 'CARBAPENEM',  id_column = 'Sample id', allele_gene_separator = '-')
head(split_data) %>% kable() %>% kable_styling() %>% scroll_box(width = "100%")
```

For microreact we want to add colour columns. Below is an example function to achieve that
```{r message=FALSE, warning=FALSE}
colour_split_data <- function(split_data, present = "red", absent = "white",  id_column = 'id'){
  # make long
  long_split_data <- split_data %>% 
    tidyr::pivot_longer(-id_column, names_to = "gene", values_to ="allele")
  # add a column for subclass__colour
  long_split_data %<>% dplyr::mutate(gene__colour = stringr::str_c(gene, "__colour"))
  # add values for colour
  long_split_data %<>% dplyr::mutate(colour = if_else(is.na(allele), absent, present))
  # pivot the subclass and gene data into wide format
  long_split_data_allele <- long_split_data %>% dplyr::select(id_column, gene, allele)
  wide_split_data_allele <- long_split_data_allele %>%
    pivot_wider(names_from = c(gene), values_from =c(allele))
  # pivot the subclass__colour and colour data into wide format
  long_split_data_allele_colour <- long_split_data %>% dplyr::select(id_column, gene__colour, colour)
  wide_split_data_allele_colour <- long_split_data_allele_colour %>%
    pivot_wider(names_from = c(gene__colour), values_from =c(colour))
  # merge the two wide tables
  coloured_data <- wide_split_data_allele %>% dplyr::left_join(wide_split_data_allele_colour, by = c(id_column))
  colnames_without_id <- sort(setdiff(colnames(coloured_data), c('id')))
  coloured_data %<>% dplyr::select(id_column, all_of(colnames_without_id))
  return(coloured_data)
}
```

To demonstrate this we will colour the data which we split into one gene per column for CARBAPENEM data
```{r message=FALSE, warning=FALSE}
coloured_split_data <- colour_split_data(split_data, id_column = 'Sample id')
head(coloured_split_data) %>% kable() %>% kable_styling() %>% scroll_box(width = "100%")
```

And finally this could be joined back to the original metadata table and the `CARBAPENEM` column dropped
```{r message=FALSE, warning=FALSE}
joined_metadata <- combined_data %>% dplyr::left_join(coloured_split_data, by = "Sample id") %>% 
  dplyr::select(-CARBAPENEM)
head(joined_metadata) %>% kable() %>% kable_styling() %>% scroll_box(width = "100%")
```
